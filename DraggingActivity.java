package com.example.tester;

import android.os.Build;
import android.os.Bundle;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.PaintDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.widget.EditText;
import android.widget.ImageView;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class DraggingActivity extends Activity {

	private ImageView mImage, mImageL, mImageR;
	private EditText mText;
	private View mMainV;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dragging);
		mMainV = ((ViewGroup)findViewById(android.R.id.content)).getChildAt(0);
		mMainV.setOnDragListener(DropListener);
		
		mImage = (ImageView)findViewById(R.id.imageD);
		mImage.setOnTouchListener(listen);
		mImageL = (ImageView)findViewById(R.id.imageL);
		mImageL.setOnDragListener(DropListener);
		mImageR = (ImageView)findViewById(R.id.imageR);
		mImageR.setOnDragListener(DropListener);
		mText = (EditText)findViewById(R.id.textDragging);
	}

	OnTouchListener listen = new OnTouchListener() {
		@Override
		public boolean onTouch(View v, MotionEvent e) {
			// Don't need ClipData, so let the parameters be blank Strings
			ClipData data = ClipData.newPlainText("", "");
			
			DragShadow dragShadow = new DragShadow(v);
			v.startDrag(data, dragShadow, v, 0);
			v.setVisibility(View.INVISIBLE);
			
			return true;
		}
	};
	
	OnDragListener DropListener = new OnDragListener() {
		@Override
		public boolean onDrag(View v, DragEvent e) {
			int dEvent = e.getAction();
			
			switch(dEvent)
			{
			case DragEvent.ACTION_DRAG_ENTERED:
				if(v.equals(mImageL))
					mText.setText("ENTERED LEFT");
				if(v.equals(mImageR))
					mText.setText("ENTERED RIGHT");
				break;
			case DragEvent.ACTION_DRAG_EXITED:
				if(v.equals(mImageL))
					mText.setText("EXITED LEFT");
				if(v.equals(mImageR))
					mText.setText("EXITED RIGHT");
				break;
			case DragEvent.ACTION_DROP:
				
				if(!v.equals(mMainV)) {
					//	x is used to specify the x-location of the drag-able image
					//		once it has been dropped
					float x = v.getX();
					
					if(v.equals(mImageL)) {
						mText.setText("DROPPED LEFT");
						
						//	The drag-able image will be slightly to the right
						x += (float)10.0;
					}
					if(v.equals(mImageR)) {
						mText.setText("DROPPED RIGHT");
						
						//	The drag-able image will be slightly to the left
						x -= (float)10.0;
					}
					
					//	The drag-able image will be slightly below the corner image
					mImage.setY(v.getY()+(float)25.0);
					mImage.setX(x);
				}
				else {
					//	Commenting out the two lines below returns the image
					//		to the previous valid location
					mImage.setX(e.getX()-(float)(mImage.getWidth()/2.0));
					mImage.setY(e.getY()-(float)(mImage.getHeight()/2.0));
				}
				
				mImage.setVisibility(View.VISIBLE);
				mImage.bringToFront();
				break;
			}
			
			return true;
		}
	};
	
	private class DragShadow extends View.DragShadowBuilder {
		private PaintDrawable shadow;
		private View v;
		
		public DragShadow(View v) {
			super(v);
			this.v = v;
			
			shadow = new PaintDrawable(Color.BLACK);
			shadow.setShape(new OvalShape());
			
			//	Set the transparency of the shadow object
			shadow.setAlpha((int)(255*0.5));
			shadow.getPaint().setAlpha(255);
		}
		
		@Override
		public void onDrawShadow(Canvas c) {
			shadow.draw(c);
		}
		
		@Override
		public void onProvideShadowMetrics(Point sSize, Point sTouchPoint) {
			//	The diameter of the shadow circle
			int width = (int)(v.getWidth()*0.5);
			
			shadow.setBounds(0, 0, width, width);
			sSize.set(width, width);
			
			//	Set to focus at the center of the circle
			sTouchPoint.set((int)(width*0.5), (int)(width*0.5));
		}
	}
}