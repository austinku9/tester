package com.example.tester;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	public void doDrag(View view) {
	    Intent intent = new Intent(this, DraggingActivity.class);
	    startActivity(intent);
	}
	
	public void doTime(View view) {
	    Intent intent = new Intent(this, TimeActivity.class);
	    startActivity(intent);
	}
}