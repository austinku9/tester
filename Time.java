package com.example.tester;

public class Time {
	private long stored, top;
	
	public Time() {
		top = -1;
	}
	
	public void clear() {
		stored = 0;
	}
	
	public String top() {
		if(top == -1)
			return "None yet";
		else
			return Long.toString(top) + " ms";
	}
	
	public String store() {
		if(stored == 0)
			stored = System.currentTimeMillis();
		else
			return diff();
		
		return null;
	}
	
	private String diff() {
		long result = System.currentTimeMillis()-stored;
		
		if(top == -1 || result < top)
			top = result;
		
		clear();
		
		return Long.toString(result) + " ms";
	}
}