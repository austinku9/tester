package com.example.tester;

import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
//import android.view.Menu;

public class TimeActivity extends Activity {

	private Time mTimer;
	private EditText mText, mTop;
	private Button mPrevButt, mCurrButt;
	private LayoutInflater mInflater;
	private PopupWindow mPop;
	private View mV;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time);
		
		//	The timing object
		mTimer = new Time();
		mText = (EditText)findViewById(R.id.textTime);
		
		//	Setting up the pop-up window
		mInflater = (LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mV = mInflater.inflate(R.layout.pop, null, false);
		mV.getBackground().setAlpha((int)(255*0.5));
		Display d = getWindowManager().getDefaultDisplay();
		Point s = new Point();
		d.getSize(s);
		mPop = new PopupWindow(mV, s.x, s.y/5, true);
		
		mTop = (EditText)mV.findViewById(R.id.textTopTime);
	}

	public void mark(View v) {
		String result;
		mCurrButt = (Button)v;
		
		//	If this is the first press
		if(mPrevButt == null) {
			mCurrButt.setText("Press the other button");
			result = mTimer.store();
			mPrevButt = mCurrButt;
		}
		else {
			if(mCurrButt.equals(mPrevButt)) {
				mText.setText("Press different buttons");
				mTimer.clear();
			}
			else {
				result = mTimer.store();
				mText.setText(result);
			}
			
			mPrevButt.setText("Press me");
			mPrevButt = null;
		}
	}
	
	public void topTimes(View v) {
		mTimer.clear();
		
		mPop.showAtLocation(this.findViewById(R.id.time_main), Gravity.CENTER, 0, 0);
		mTop.setText(mTimer.top());
	}
	
	public void closeTop(View v) {
		if(mCurrButt != null)
			mCurrButt.setText("Press me");
		if(mPrevButt != null)
			mPrevButt.setText("Press me");
		
		mPop.dismiss();
	}
}